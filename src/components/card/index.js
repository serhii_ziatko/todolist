import ng from 'angular';

import CardComponent from './card.component';
import TodoService from '../todo.service';
import TodoItem from '../todoItem';

export default ng.module('app.components.card', [TodoItem])
  .service('TodoService', TodoService)
  .component('card', CardComponent)
  .name;
