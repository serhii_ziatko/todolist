import template from './card.html';
import controller from './card.controller';

export default {
  template,
  controller,
  bindings: {
    card: '<',
    cardTitle: '<',
    deleteCard: '&',
  }
};
