export default class TodoController {
  constructor(TodoService) {
    'ngInject';
    this.TodoService = TodoService;
    this.allComplited = false;
    this.background = `rgba(${Math.floor(Math.random() * 256)}, ${Math.floor(Math.random() * 256)}, ${Math.floor(Math.random() * 256)}, ${Math.floor(Math.random() * 100) / 100})`;
    this.swalTemplate = {
      title: 'Are you sure?',
      text: 'You will not be able to recover this todo!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel plx!',
      closeOnConfirm: false,
      closeOnCancel: false
    };
  }

  $onInit() {
    this.onInit();
  }

  onInit() {
    this.todos = this.card.todos;
    this.allComplite();
  }

  addTodo() {
    this.card.todos.push({ title: this.title || '', isComplete: false });
    this.TodoService.save(this.card).then(this.onInit.bind(this));
    this.title = '';
  }

  removeTodo(index) {
    swal(this.swalTemplate, (isConfirm) => {
      if (isConfirm) {
        this.card.todos.splice(index, 1);
        this.TodoService.save(this.card).then(this.onInit.bind(this));

        swal('Deleted!', 'Your todo has been deleted.', 'success');
      } else {
        swal('Cancelled', 'Your todo is safe :)', 'error');
      }
    });
  }

  complitedTodo(index) {
    this.card.todos[index].isComplete = !this.card.todos[index].isComplete;
    this.TodoService.save(this.card).then(this.onInit.bind(this));
  }

  editTodo(index, title) {
    this.card.todos[index].title = title;
    this.TodoService.save(this.card).then(this.onInit.bind(this));
  }

  delCard() {
    this.deleteCard();
  }

  allComplite() {
    if (this.card.todos.length) {
      const index = this.card.todos.findIndex(item => item.isComplete === false);
      if (index < 0) {
        this.allComplited = true;
        return;
      }
      this.allComplited = false;
    } else {
      this.allComplited = false;
    }
  }
}
