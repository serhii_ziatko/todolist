export default class TodoService {
  constructor($http) {
    'ngInject';
    this.$http = $http;
  }

  list() {
    return this.$http.get(`${API}/cards`).then(result => result.data);
  }

  detail(id) {
    return this.$http.get(`${API}/cards/${id}`).then(result => result.data);
  }

  add(data) {
    return this.$http.post(`${API}/cards/`, data).then(result => result.data);
  }

  update(data) {
    return this.$http.put(`${API}/cards/${data.id}`, data).then(result => result.data);
  }

  remove(id) {
    return this.$http.delete(`${API}/cards/${id}`);
  }

  save(data) {
    if (data.id) {
        return this.update(data);
    }
    return this.add(data);
  }

}
