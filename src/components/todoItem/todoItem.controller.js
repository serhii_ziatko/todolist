export default class TodoItemController {
  constructor(TodoService) {
    'ngInject';
    this.TodoService = TodoService;
  }
  edit() {
    delete this.todo.$edit;
    this.editTodo();
  }

  focusInput(e, selector) {
    angular.element(selector).trigger('focus');
  }
}
