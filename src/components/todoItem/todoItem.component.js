import template from './todoItem.html';
import controller from './todoItem.controller';

export default {
  template,
  controller,
  bindings: {
    todo: '<',
    removeTodo: '&',
    complitedTodo: '&',
    editTodo: '&',
  }
};
