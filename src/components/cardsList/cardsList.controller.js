export default class TodoListController {
  constructor(TodoService) {
    'ngInject';
    this.TodoService = TodoService;
    this.isShow = true;
    this.swalTemplate = {
      title: 'Are you sure?',
      text: 'You will not be able to recover this card!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel plx!',
      closeOnConfirm: false,
      closeOnCancel: false
    };
  }
  $onInit() {
    this.onInit();
  }
  onInit() {
    this.TodoService.list().then((cardList) => {
      this.cardList = cardList;
    }).then(() => {
      if (this.cardList.length >= 3) {
        return this.isShow = false;
      }
      return this.isShow = true;
    });
  }

  deleteCard(id) {
    swal(this.swalTemplate, (isConfirm) => {
      if (isConfirm) {
        this.TodoService.remove(id).then(this.onInit.bind(this));

        swal('Deleted!', 'Your card has been deleted.', 'success');
      } else {
        swal('Cancelled', 'Your card is safe :)', 'error');
      }
    });
  }
  addCard() {
    this.TodoService.save({ title: 'Todo title', todos: [] }).then(this.onInit.bind(this));
  }
}
