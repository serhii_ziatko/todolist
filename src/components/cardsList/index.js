import ng from 'angular';

import CardListComponent from './cardsList.component';
import TodoService from '../todo.service';


export default ng.module('app.components.cardsList', [])
  .service('TodoService', TodoService)
  .component('cardsList', CardListComponent)
  .name;
